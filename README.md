# Diagrams as Code



## Mermaid

Should be natively supported in GitLab code blocks.

Below should be a flowchart:

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```

Above should be a flowchart
